# Electroencephalography Classification First Part

This is a master project from Vahap and Sushant, master students at Uni Bremen. This project is tutored by Yarib Nevarez.

The project aims to develop and implement EEG signal classifier for detection of epileptic seizures.

The project software consists of two parts. This project is the first part (**Eeg-Classification-Network-Builder**). The first part is to build a neural network by using python language to classify EEG signals.  

The second part implements the neural network which is built in the first part, to perform classification. It is written in C language to be installed in FPGA board. The second part of the project is **Eeg-Classification** and can be found [here](https://gitlab.com/abdulvahap/eeg-classification).

## What to do 

1. Run **NeuralNetworkClass.py** to create a neural network and export network parameters together with test data.
2. Copy exported csv files and paste them in [the second project](https://gitlab.com/abdulvahap/eeg-classification) to perform classification on test data.
3. Go to second part and run C code to perform classification.